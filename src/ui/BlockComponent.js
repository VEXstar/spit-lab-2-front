import * as React from "react";
import {Card} from "antd";

export class BlockComponent extends React.Component{

    render() {
        return(
            <Card title={this.props.link} extra={<a href={this.props.link}>Перейти</a>}>
                {this.props.desc}
            </Card>
        )
    }
}