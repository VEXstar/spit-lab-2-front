import * as React from "react";
import {Button, Checkbox, Dropdown, Input, Menu, Modal, Select} from "antd";
import {add, find} from "../lgoic/FindLogic";
import DownOutlined from "@ant-design/icons/lib/icons/DownOutlined";
import {BlockComponent} from "./BlockComponent";
import PlusCircleTwoTone from "@ant-design/icons/lib/icons/PlusCircleTwoTone";
const { Search,TextArea } = Input;
const {Option} = Select;
export class SearchComponent  extends React.Component{
    state = {
        onSearch: false,
        srcLabel: "Все",
        typeLabel:"Любой",
        content:"",
        visible: false
    };
    srcs = null;
    tpss = null;

     onSrch = term => {
        this.setState({ onSearch:true });
        find(term,this.srcs,this.tpss).then(req=>{
            console.log(req);
            const poper = req.map((el)=><BlockComponent title={"test"} desc={el.desc} link={el.link}/>);
            this.setState({content:poper});
            this.setState({ onSearch:false })
        })
        ;
    };
    onChangeSrc(checkedValues) {
        if(checkedValues.length===0)
            this.setState({srcLabel:"Все"});
        else
            this.setState({srcLabel:"Особые"});
        this.srcs = checkedValues;
    }

    onChangeTyp(checkedValues) {
        if(checkedValues.length===0)
            this.setState({typeLabel:"Любой"});
        else
            this.setState({typeLabel:"Особые"});
        this.tpss = checkedValues;
    }

    srcOpts = [
        { value: 'fips', label: 'FIPS' },
        { value: 'lib', label: 'elibrary' },
        { value: 'plus', label: 'Консультатнт плюс' },
        { value: 'net', label: 'Интернет' },
    ];

    tpsOpts = [
        { value: 'patent', label: 'Патент' },
        { value: 'publication', label: 'Публикация' },
        { value: 'right', label: 'Правовой' },
        { value: 'paper', label: 'Статья' },
    ];

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleOk = e => {
        if(this.text!==null&&this.link!==null){
                add(this.text, this.src, this.type, this.link).then((r)=>{
                    if(!r)
                        alert("Что то пошло не так!");
                    this.setState({
                        visible: false,
                    });
                });
        }
    };

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };


     renderSources(){
         return <Menu>
            <div>
                <Checkbox.Group options={this.srcOpts} onChange={(s)=>this.onChangeSrc(s)} />
            </div>
         </Menu>
     }

    renderTypes(){
        return <Menu>
            <div>
                <Checkbox.Group options={this.tpsOpts} onChange={(s)=>this.onChangeTyp(s)} />
            </div>
        </Menu>

    }
    render() {
        return(
            <div>
                <Input.Group compact>
                    <div className="colmun">
                        <span>Источник:</span>
                        <Dropdown overlay={this.renderSources()}>
                            <Button>
                                {this.state.srcLabel}<DownOutlined />
                            </Button>
                        </Dropdown>
                    </div>
                    <div className="colmun">
                        <span>Тип документа:</span>
                        <Dropdown overlay={this.renderTypes()}>
                            <Button>{this.state.typeLabel}<DownOutlined />
                            </Button>
                        </Dropdown>
                    </div>
                    <div className="colmun2">
                        <span>Запрос:</span>
                        <Search placeholder="input search text" onSearch={this.onSrch} loading={this.state.onSearch} enterButton />
                    </div>
                    <div className="clmn3">
                        <span> </span>
                        <div>
                            <Button type="primary" onClick={this.showModal}>
                                <PlusCircleTwoTone />
                            </Button>
                            <Modal
                                title="Добавление новой ссылки"
                                visible={this.state.visible}
                                onOk={this.handleOk}
                                onCancel={this.handleCancel}
                            >
                                <div className="onlyder">
                                    <span>Источник:</span>
                                    <Select onSelect={(val)=>this.setAddSource(val)} defaultValue="net" style={{ width: '30%' }}>
                                        <Option value="fips">ФИПС</Option>
                                        <Option value="lib">eLIBRARY</Option>
                                        <Option value="plus">Консультат +</Option>
                                        <Option value="net">Интернет</Option>
                                    </Select>
                                </div>
                                <div className="onlyder">
                                    <span>Тип:</span>
                                    <Select onSelect={(val)=>this.setAddType(val)} defaultValue="paper" style={{ width: '30%' }}>
                                        <Option value="patent">Патент</Option>
                                        <Option value="publication">Публикация</Option>
                                        <Option value="right">Правовой</Option>
                                        <Option value="paper">Статья</Option>
                                    </Select>
                                </div>
                                <div className="onlyder">
                                    <span>Ссылка:</span>
                                    <Input onChange={(s)=>this.setAddLink(s.target.value)} placeholder="Ссылка" />
                                </div>
                                <div className="onlyder">
                                    <span>Описание:</span>
                                    <TextArea onChange={(s)=>this.setAddText(s.target.value)} rows={4}  placeholder="Ключевые слова" />
                                </div>

                            </Modal>
                        </div>
                    </div>
                </Input.Group>
                <div style={{padding: "20px"}}>
                    {this.state.content}
                </div>
            </div>
        )
    }
    text = null;
    setAddText(text){
        this.text = text;
    }

    link = null;
    setAddLink(link){
        this.link = link;
    }
    src = "net";
    setAddSource(src){
         this.src = src;
    }
    type = "paper";
    setAddType(type){
        this.type = type;
    }

}