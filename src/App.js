import React from 'react';
import './App.css';
import { Layout} from 'antd';
import 'antd/dist/antd.css';
import {SearchComponent} from "./ui/SearchComponent";
const { Header, Content, Footer } = Layout;
function App() {

  return (
    <div className="App">
      <Layout className="site-layout">
        <Header className="site-layout-background">
          Информационно-Поисковая система по тематике сотовых сетей
        </Header>
        <Content className="content">
         <SearchComponent/>
        </Content>
        <Footer style={{textAlign: "center"}}>
            Лабораторная работа №2 по дисциплине СПИТ, Ахметшин Аскар МО-316, 2020
        </Footer>
      </Layout>
    </div>
  );
}

export default App;
