
export async function find(term,sources,types) {
    if(term==null)
        return null;
    let srcs = "";
    if(sources!=null&&sources.length!==0)
        srcs+="&source=" + sources.join(",");
    if(types!=null&&types.length!==0)
        srcs+="&doctypes=" + types.join(",");
    let url = "/spitcore/find?term="+term+srcs;
    console.log(url);
    return  await fetch(url).then((res)=>res.json());
}
export async function add(terms,source,type,link) {
    const formData = new FormData();
    formData.append('link', link);
    formData.append('desc', terms);
    formData.append('doctype', type);
    formData.append('source', source);
    let url = "/spitcore/add";
    console.log("asd");
    return  await fetch(url,{
        method: 'PUT',
        body: formData}).then((res)=>res.json());
}
window.$$fnd = find;